# README #

The sample is setup to be run from Node.js. You can either run server.js in the root directory with node.exe, or use nodemon.

### What is this repository for? ###

* A simple sample application which mocks up an Angular responsive web application and demonstrates ngAnimate, angular ui router, moment and $interval.
* 1.1

### Who do I talk to? ###

* rod@hatchinnovations.com