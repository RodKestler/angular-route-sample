// Script for forecast controller - this root list of forecast items
(function (){
    'use strict';
    angular.module('app').controller('forecast', ['$scope', '$state', '$rootScope',
    '$interval', 'WeatherService',
    function ($scope, $state, $rootScope, $interval, WeatherService) {
        var model = this;

        var windDirs = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'];

        function formatWind(item) {
            var txt = Math.round(item.speed);
            txt += " miles/hour from ";
            var index = Math.round((Math.round(item.deg + 22.5) % 360) / 45) % 8;
            txt += (index >= 0 && index < 8 ? windDirs[index] : "");
            return txt;
        }

        function formatItem(dataItem, index) {
          var weather = dataItem.weather[0];
          var date = new Date();
          date.setTime(dataItem.dt * 1000);
          var mom = moment(date);
          var boundItem = {
              date: date,
              index: index,
              dayHeader: mom.format("MMMM Do"),
              dayName: (index == 1 ? "Tomorrow" : mom.format("dddd")),
              high: Math.round(dataItem.temp.max),
              low: Math.round(dataItem.temp.min),
              smallIcon: "/images/ic_" + weather.main + ".png",
              bigIcon: "/images/art_" + weather.main + ".png",
              state: weather.main,
              humidity: dataItem.humidity,
              pressure: Math.round(dataItem.pressure),
              wind: formatWind(dataItem),
              clouds: dataItem.clouds
          };
          return boundItem;
        }

        function onRefresh() {
          console.log("refeshing forecast at " + new Date());
          WeatherService.getForecast("Atlanta,ga").then(function(result) {
              $scope.items = [];

              if (result.list.length > 0) {
                  //format bindable items
                  $.each(result.list, function(index, item)
                  {
                      if (index == 0) {
                        $scope.today = formatItem(item, index);
                      }
                      else {
                        var dayItem = formatItem(item, index);
                        $scope.items.push(dayItem);
                      }
                  });
              }
          });
        }

        $scope.onItemClicked = function(item) {
          $scope.transition = 'slide-left';
          $rootScope.selectedItem = item;
          $state.go("detail");
        }

        //first refresh
        onRefresh();

        //set the refresh interval for 10 minutes
        $interval(onRefresh, 10 * 60 * 1000);
    }
]);

})();
