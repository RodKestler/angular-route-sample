angular.module('app').directive("forecastItem", ['$state',
    function ($state) {
    return {
        restrict: "A",
        scope: {
            item: '=',
            onItemClicked: '&'
        },
        templateUrl: "/app/directives/forecastItem.html",
        link: function (scope, element, attrs) {
          scope.showDetail = function() {
            if (scope.onItemClicked) scope.onItemClicked({ item: scope.item });
          }
        }
    }
}]);
