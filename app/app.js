angular.module('app', [
    'ngAnimate',
    'ui.router'
]).filter('trustedHtml', ['$sce', function ($sce) {
    return function (html) {
        return $sce.trustAsHtml(html);
    }
}]).filter('trustAsResourceUrl', ['$sce', function ($sce) {
    return function (url) {
        return $sce.trustAsResourceUrl(url);
    };
}]).run(function ($rootScope, $anchorScroll, $http, $state) {
    //Do any run-time initialization here
    $state.go("forecast");
});

//Putting configuration here - normally, I would put this in a separate script file
//Configuration
angular.module('app').config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', '$animateProvider',
    function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $animateProvider)
    {
        $urlRouterProvider.otherwise('/');
        $locationProvider.html5Mode(true);
        $animateProvider.classNameFilter(/^(?:(?!ng-animate-disabled).)*$/);
        
        $stateProvider
            .state('forecast', {
                url: '/forecast',
                templateUrl: '/app/forecast.html'
            }).state('detail', {
                url: '/detail',
                templateUrl: '/app/detail.html'
            });
    }
]);
