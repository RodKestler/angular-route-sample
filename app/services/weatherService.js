(function () {
    'use strict';

    angular.module('app').service('WeatherService', ['$http', '$q', 
    function ($http, $q) {
        var instance = {
            getForecast: getForecast
        };

        return instance;

        //http://api.openweathermap.org/data/2.5/forecast/daily?q=Atlanta,ga&mode=json&cnt=5&units=imperial&apikey=3aa158b2f14a9f493a8c725f8133d704
        function getForecast(location){
            var deferred = $q.defer();
            
            //Normally, the API key would coe from configuraion, and I would implement this service on the backend so that the API key could be obfsucated
            var uri = "http://api.openweathermap.org/data/2.5/forecast/daily?q=" + location + "&mode=json&cnt=6&units=imperial&apikey=3aa158b2f14a9f493a8c725f8133d704";
            $http.get(uri).success(function (data) {
                deferred.resolve(data);
            });

            return deferred.promise;
        }
    }
]);

})();