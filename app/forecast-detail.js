// Script for forecast detail controller - this is the drill-in detail route/view
(function (){
    'use strict';
    angular.module('app').controller('forecastDetail', ['$scope', '$rootScope', '$state',
    function ($scope, $rootScope, $state) {
      $scope.item = $rootScope.selectedItem;
      if (!$rootScope.selectedItem) {
        $state.go("forecast");
      }
    }
  ]);
})();
