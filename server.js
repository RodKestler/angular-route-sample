var fs = require('fs'),
    http = require('http');

http.createServer(function (req, res) {
  var pathName = req.url;

  //Brutish redirects
  if (!pathName || pathName == "" || pathName == "/"
    || pathName == "/forecast" || pathName == "/detail") pathName = "/index.html";

  fs.readFile(__dirname + pathName, function (err,data) {
    if (err) {
      res.writeHead(404);
      res.end(JSON.stringify(err));
      return;
    }
    res.writeHead(200);
    res.end(data);
  });
}).listen(8000);

console.log("Started Sample App on port 8000...");
